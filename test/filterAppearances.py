import datetime

nan = 'foo'
appearances = [{'AppearanceDate': '11/24/2020', 'Time': '02:00 PM', 'Purpose': 'Motion', 'OutcomeType': nan, 'JudgePart': 'Honorable Bruce E. Scheckowitz', 'MotionSeq': 1.0}, {'AppearanceDate': '11/10/2020', 'Time': '10:00 AM', 'Purpose': 'Motion', 'OutcomeType': 'Reserved Decision', 'JudgePart': 'Honorable Bruce E. Scheckowitz', 'MotionSeq': 1.0}, {'AppearanceDate': '10/06/2020', 'Time': '11:30 AM', 'Purpose': 'Motion', 'OutcomeType': 'Adjourned', 'JudgePart': 'Honorable Bruce E. Scheckowitz', 'MotionSeq': 1.0}, {'AppearanceDate': '03/13/2020', 'Time': '10:30 AM', 'Purpose': 'For All Purposes', 'OutcomeType': 'Judgment with Possession, with Warrant, No Stay/Issuance Forthwith, Execution Stayed per Stipulation/Order, Earliest Execution Date 04/01/2020', 'JudgePart': 'Honorable David Alan Harris\xa0Part C', 'MotionSeq': nan}, {'AppearanceDate': '02/18/2020', 'Time': '10:30 AM', 'Purpose': 'For All Purposes', 'OutcomeType': 'Adjourned', 'JudgePart': 'Honorable Eleanora Ofshtein\xa0Part C', 'MotionSeq': nan}]

filters = {'OutcomeType': 'Reserved', 'OutcomeType': 'Granted', 'AppearanceDate': datetime.datetime.today()}

def filterAppearances(appearances, filters):
    output = []
    for a in appearances:
        if len(filters) >= 1:
            for key, value in filters.items():
                if isinstance(value, datetime.date):
                    appearance_date = datetime.datetime.strptime(a[key], '%m/%d/%Y')
                    print('apperance date: ', appearance_date)
                    if appearance_date.date() > value.date():
                        output.append(a)
                    continue
                if isinstance(a[key], str) and value in a[key]:
                    output.append(a)
                    continue
        else:
            output.append(a)
    return output
filterAppearances(appearances, filters)
