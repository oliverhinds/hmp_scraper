# currently not in use!

from bs4 import BeautifulSoup
import requests
import re
from datetime import datetime as dt
import dateparser
import pandas as pd
# from gsheetstools import gSheet
import sys
sys.path.append('./code')
from case_class import caseScrape

# for grabbing stray tenant info
def getAddresses(indexes_to_scrape):
    if not indexes_to_scrape:
        return pd.DataFrame()

    else:
        scraper = caseScrape()
        entries = []
        for n, c in enumerate(indexes_to_scrape):
            print('getting info for case {} of {}'.format(str(n), str(len(indexes_to_scrape))))
            
            scraper.captchaCheck()
            scraper.getCaseData(c)

            dicto_ = {
                'Case Index': c,
                'Tenant Address':scraper.address,
                'Tenant Name':scraper.tenant_name,
                'Docket ID':scraper.docket_id
            }

            print(dicto_)
            entries.append(dicto_)
        
        scraper.quit()

        entries_df = pd.DataFrame(entries)
        return entries_df

def updateWatchList(test=False):
    # # initiate sheet
    # if not test:
    #     sheet_id = '1EVH20l_m_yW8KU8nlY0fPEnc6WMNlq9Z4HHMwDcBFfc' # prod sheet id
    # else:
    #     sheet_id = "1fEgDiGTEV8TgyiwluuG60PKSAvLYTu7s8Gw9uc9wrmw" # test sheet id
    # g = gSheet(sheet_id, creds_file="credentials_service.json")

    # # load appearances (you just ran a scrape, the data is already added)
    # appearances = g.loadDataFromSheet('Appearance')


    # # load old watch list (all appearances)
    # old_watch_list = g.loadDataFromSheet('Watch List (All Appearances)')
    # if old_watch_list is not None:
    #     old_watch_list['AppearanceDate'] = pd.to_datetime(old_watch_list['AppearanceDate'])
    # else:
    #     old_watch_list = pd.DataFrame()

    # subset cases with future dates in HMP1/2 and any with 'reserved decision' as outcome
    indexes_to_watch = appearances[
        (appearances['Part'].str.contains('HMP')) & (appearances['AppearanceDate'].map(lambda d: dateparser.parse(d)) > dt.now())]['Index Number:'].to_list() + appearances[appearances['OutcomeType'] == 'Reserved Decision']['Index Number:'].to_list()

    # UPDATE TENANT INFO
    # download and update tenant info
    tenant_info_old = g.loadDataFromSheet('Tenant Info')
    if tenant_info_old is not None:
        tenant_info_old = tenant_info_old.drop_duplicates()
        indexes_to_scrape = list(set([i for i in indexes_to_watch if i not in tenant_info_old['Case Index'].values]))
    else:
        indexes_to_scrape = indexes_to_watch
    
    # get new tenant info and update sheet and local info ('all_tenant_info')
    print('scraping info for {} cases...'.format(str(len(indexes_to_scrape))))
    tenant_info_new = getAddresses(indexes_to_scrape)
    tenant_info_new = tenant_info_new.drop_duplicates()

    print('tenant info old: ' + str(len(tenant_info_old)))
    print('tenant info new: ' + str(len(tenant_info_new)))

    if tenant_info_old is not None:
        all_tenant_info = tenant_info_old
    else:
        all_tenant_info = pd.DataFrame(columns=['Case Index', 'Tenant Address', 'Tenant Name', 'Docket ID'])

    if len(tenant_info_new):
        tenant_info_new = tenant_info_new[tenant_info_new['Case Index'] != 'Case Index']
        g.writeDataToSheet(tenant_info_new, 'Tenant Info', write_setting='append')
        all_tenant_info = all_tenant_info.append(tenant_info_new)
    
    print('all tenant info: ' + str(len(all_tenant_info)))

    # ADD MORE DATA
    # subset watched cases, cols and merge with tenant info
    cols = ['Index Number:', 'AppearanceDate', 'Time', 'Part', 'OutcomeType', 'Judge', 'Case Name:', 'Defendant Atty']
    appearances_cut = appearances[appearances['Index Number:'].isin(indexes_to_watch)][cols]
    appearances_final = pd.merge(appearances_cut, all_tenant_info, left_on=('Index Number:'), right_on=('Case Index'), how='left')
    appearances_final.drop('Case Index', axis=1, inplace=True)

    # write Reserved Decisions to sheet
    rd_cases = appearances_final[appearances_final['OutcomeType'] == 'Reserved Decision']['Index Number:'].to_list()
    old_rd = g.loadDataFromSheet('Watch List (Reserved Decisions)')
    rd_df = appearances_final[appearances_final['Index Number:'].isin(rd_cases)]
    rd_df = rd_df.append(old_rd)
    rd_df['AppearanceDate'] = pd.to_datetime(rd_df['AppearanceDate'])
    rd_df = rd_df.drop_duplicates()
    rd_df.sort_values('AppearanceDate', ascending=False, inplace=True)

    rd_df['AppearanceDate'] = rd_df['AppearanceDate'].astype(str)
    g.writeDataToSheet(rd_df, 'Watch List (Reserved Decisions)')

    # EDIT ALL APPEARANCES WATCH LIST
    # remove reserved decisions and add old watch list
    appearances_final = appearances_final.append(old_watch_list)
    appearances_final = appearances_final[~appearances_final['Index Number:'].isin(rd_cases)]
    appearances_final['AppearanceDate'] = pd.to_datetime(appearances_final['AppearanceDate'])
    appearances_final.drop_duplicates(subset=['Index Number:', 'AppearanceDate', 'Time'], inplace=True)
    appearances_final_sorted = appearances_final.sort_values(
        ['Index Number:', 'AppearanceDate'], ascending=False)
        
    # add Case Order
    appearances_final_sorted['Case Order'] = appearances_final_sorted.groupby('Index Number:').cumcount()

    # add Document Link
    appearances_final_sorted['Document Link'] = appearances_final_sorted['Docket ID'].map(
        lambda d: "https://iapps.courts.state.ny.us/nyscef/DocumentList?docketId={}".format(d))

    print(appearances_final_sorted.shape)

    # write All Appearances to sheet
    appearances_final_sorted = appearances_final_sorted.sort_values(
        ['Index Number:', 'AppearanceDate'], ascending=False)
    appearances_final_sorted['AppearanceDate'] = appearances_final_sorted['AppearanceDate'].astype(str)
    g.writeDataToSheet(appearances_final_sorted, 'Watch List (All Appearances)')

    # MAKE NEXT/PREV TABLE
    # subset only HMP cases
    appearances_final_hmp_only = appearances_final_sorted[appearances_final_sorted['Part'].str.contains('HMP')]
    # subset and combine previous dates
    case_cols = ['Index Number:', 'AppearanceDate', 'Time', 'OutcomeType', 'Judge', 'Case Order']
    prev_dates = appearances_final_hmp_only[
        appearances_final_hmp_only['Case Order'] == 1][case_cols].set_index('Index Number:')

    appearances_final_shaped = appearances_final_hmp_only[
        appearances_final_hmp_only['Case Order']==0
        ].set_index('Index Number:'
        ).merge(prev_dates, on='Index Number:', suffixes=('_next', '_prev')
        )

    # re-sort ("AppearanceDate_next" should be datetime!)
    appearances_final_shaped = appearances_final_shaped.sort_values('AppearanceDate_next', ascending=False)
    appearances_final_shaped = appearances_final_shaped.reset_index()
    cols = [
        'Index Number:', 'Tenant Name', 'Tenant Address', 'Case Name:', 'Defendant Atty',
        'AppearanceDate_next', 'Time_next', 'OutcomeType_next', 'Judge_next',
        'AppearanceDate_prev', 'Time_prev', 'OutcomeType_prev', 'Judge_prev', 'Document Link'
    ]
    appearances_final_shaped = appearances_final_shaped[cols]

    for d in [c for c in appearances_final_shaped.columns if 'AppearanceDate' in c]:
        appearances_final_shaped[d] = appearances_final_shaped[d].astype(str)
    g.writeDataToSheet(appearances_final_shaped, 'Watch List (Last/Next)')







