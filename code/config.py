import os
import json

class Config:
    def __init__(self):
        if os.name == 'nt':
            self.dir_ = os.environ["APPDATA"] + 'hmp_scraper_sql/'

        else:
            self.dir_ = os.path.expanduser('~/') + 'Documents/code/BED/hmp_scraper_sql/'
        if "HEROKU_TEST" not in os.environ:
            config_ = json.load(open(self.dir_ + 'code/config.json', 'rb'))
            for k in config_:
                exec('self.{} = config_["{}"]'.format(k, k))

        else:
            for k in os.environ:
                exec('self.{} = os.environ["{}"]'.format(k, k))

