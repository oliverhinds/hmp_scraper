# Housing Motion Part Scraper
A Python script that makes looking up data on Housing Motion Part cases easy and fun.

Court data, like so many public documents, is technically "public information", but actually getting to it involves navigating lots of trash government websites. This script does all of that for you, gathering the info you need while you look at TikTok or whatever. All you have to do is feed it information and handle any CAPTCHAs it encounters.

## Contents
1. **[Installation](#installation)**

	1. [Download link](#download) (for advanced users)
	2. [Installation (Mac)](#mac)
	3. [Installation (PC)](#pc)
2. **[User's Guide](#usersguide)**

	1. [Calendar Scraper](#cal)
	2. [Case Info Scraper](#case)
	3. [Updating Code](#update)
3. **[Troubleshooting](#troubleshooting)**


<a name="installation"></a>
# Installation

<a name="download"></a>
## Download Link

If you already have Python on your machine and you don't need help finding and installing packages out on the internet, [here's the GitLab repo link](https://gitlab.com/afriedman412/hmp_scraper).


<a name="mac"></a>
## Installation Instructions (Mac)
The majority of this is **running commands on Terminal.** If you don't know what that is, that's totally fine. It's already on your computer! Type command-space bar and type in 'Terminal' to load it up.

1. **Install [Homebrew](https://brew.sh/)**  
Copy the code from the Homebrew page and run it on Terminal

	*Homebrew makes installing things easy and assures nothing you are installing will break anything else.*

2. **Install Python**  
Run `brew install python`

	*Python is the programming language that we wrote all the code in.*

3. **Install Geckodriver**  
Run `brew install geckodriver`

	*Geckodriver drives a web browser so you don't have to.*

4. **Install Firefox (if you don't already have it)**  
Run `brew install --cask firefox`

	*If they don't remember when it was called Netscape, they are too young for you.*

5. **Download the HMP Scraper**  
• Run `cd ~/Documents` to navigate to your Documents folder  
• Run `git clone git@gitlab.com:afriedman412/hmp_scraper.git` to download the code to your computer

	**... If that doesn't work...**  
	• [Download it from the GitLab repo](https://gitlab.com/afriedman412/hmp_scraper) by loading up that link, clicking on the download button (it's next to the blue "Clone" button) and choosing 'zip'  
	• Move the downloaded .zip file to your Documents folder and unzip it  

6. **Install Python packages**  
• Run `cd ~/Documents/hmp_scraper-master` to navigate to the right folder  
• Run `pip3 install -r requirements.txt` to install all the extra Python code needed to run the Scraper

Remember where this folder is, because this is where the script lives!

### You are done!


<a name="pc"></a>
## Installation Instructions (PC)
I am a Mac person and I am copying and pasting instructions from the internet for all these steps, so if anything doesn't work please let me know!

1. **Install [Python](https://realpython.com/installing-python/#how-to-install-from-the-microsoft-store)**  
I think the Microsoft Store version of Python should be fine for now, but let me know if it is not!

	*Python is the programming language that we wrote all the code in.*

2. **Download Geckodriver**  
• [Download the latest version of Geckodriver here](https://github.com/mozilla/geckodriver/releases) -- use the link that   has "win-64" in it  
• Unzip the .zip file  

	*Geckodriver drives a web browser so you don't have to.*

3. **Install Geckodriver** (annoying but not difficult)  
• Copy the path to the Geckodriver folder -- right-click on the folder, choose "Properties" and copy what's next to "Location:"  
• Right-click on My Computer or This PC   
• Select Properties  
• Select "Advanced system settings"  
• Click on "Environment Variables"  
• Under "System Variables" select PATH 
• Click "Edit", then click "New"  
• Paste the path of Geckodriver folder (you it copied earlier)

4. **Install [Firefox](https://www.mozilla.org/en-US/firefox/new/) (if you don't have it)**

5. **Download the HMP Scraper**  
• Run `cd ~\Documents` on the command line to navigate to your Documents folder  
• Run `git clone git@gitlab.com:afriedman412/hmp_scraper.git` to download the code to your computer

	**... If that doesn't work...**  
• [Download it from the GitLab repo](https://gitlab.com/afriedman412/hmp_scraper) by loading up that link, clicking on the download button (it's next to the blue "Clone" button) and choosing 'zip'  
• Move the downloaded .zip file to your Documents folder and unzip it  

6. **Install Python packages**  
• Run `cd ~\Documents\hmp_scraper-master` to navigate to the right folder  
• Run `pip3 install -r requirements.txt` to install all the extra Python code needed to run the Scraper

Remember where this folder is, because this is where the script lives!

### You are done!

<a name="usersguide"></a>
# User's Guide


<a name="cal"></a>
## Calendar Scraper
Loads cases and scrapes case information by date.  

### Instructions
* In the **hmp_scraper-master** folder, open up **calendar_scraper.py** in any old text editing program.  
*I like [Sublime](https://www.sublimetext.com/) but TextEdit or whatever is totally fine.*
* Choose the **court** by typing "kings" or "queens".  
*All lower-case -- Python is case-sensitive!*
* Enter a **start date** and **end date** in the format "YYYY-MM-DD" (including the quotation marks)  
* Leave the **end date** blank to scrape everything to the present
* Leave both blank to scrape everything, possibly back to January 2020?  
*If you try this, tell me how it goes!*
* Open Terminal/Shell, navigate to the code folder (Mac - `cd ~/Documents/hmp_scraper-master`, PC - `cd ~\Documents\hmp_scraper-master`)
* Run `python3 calendar_scraper.py` and watch it go! Firefox should open.

The site will make you do a CAPTCHA. Once you get through that, it will (should) run on its own until it decides to throw another CAPTCHA up. The scraper will wait for you to handle that until it continues. When it's done, the program should close and quit Firefox.

### Output
At the moment, the output is two files in the main folder: **cases\_temp.csv** and **case\_details\_temp.csv**. Soon, there will also be **appearances\_temp.csv**. Less soon, these documents will be collated into one, and probably uploaded to your Google Drive automatically.

CSV files are just basic spreadsheet docs (it stands for Comma Separated Values) and you can open them with Excel, Numbers or Google Sheets.

**Warning:** Every time you run the scraper, it will write new data to these files (and write over whatever was there before it ran) unless you rename them or move them to a different folder. I'll fix this later! But heads up.

<a name="cal"></a>
## Court Info Scraper
Loads cases and scrapes case information by case ID.  

### Instructions
* In the **hmp_scraper-master** folder, open up **court_info_scraper.py** in any old text editing program.  
*I like [Sublime](https://www.sublimetext.com/) but TextEdit or whatever is totally fine.*
* Put as many case IDs as you want. Each ID should be in quotation marks and separated by a comma.  
Like this: ["LT-085155-19/KI", "LT-073461-19/KI"]
* Open Terminal/Shell, navigate to the code folder (Mac - `cd ~/Documents/hmp_scraper-master`, PC - `cd ~\Documents\hmp_scraper-master`)
* Run `python3 court_info_scraper.py` and watch it go! Firefox should open.

The site will make you do a CAPTCHA. Once you get through that, it will (should) run on its own until it decides to throw another CAPTCHA up. The scraper will wait for you to handle that until it continues.

When it's done, the program should close and quit Firefox.

### Output
The scraper will make a new folder for each case and download every document available. It will also generate a summary file with the information about all of the documents.


<a name="update"></a>
## Updating Code
When we update the code, you can automatically update the local copy of the code. Here's how!

1. Navigate to the main folder (Mac - `cd ~/Documents/hmp_scraper-master`, PC - `cd ~\Documents\hmp_scraper-master`)
2. Run `git pull`


<a name="troubleshooting"></a>
# Troubleshooting
If either scraper seems like it stopped but Firefox isn't frozen, here's what you should do:

1. Check the other Firefox windows for a CAPTCHA. The script won't move until that's handled.
2. If there's no CAPTCHA, check the Terminal and make sure it's not just pausing to let things load.
3. If it doesn't say something like "waiting...", it will probably have thrown an error. If you want, take a screen shot and send it to me and I'll see if we can get things sorted.